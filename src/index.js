import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from './components/login';
import dashboard from './containers/dashboard'
import usersList from './containers/users-list';
import resourceList from './containers/resource-list';
import expenseList from './containers/expense-list';
import createUser from './containers/create-user';
import createResource from './containers/create-resource';
import createExpense from './containers/create-expense';
import deleteUser from './containers/delete-user';
import deleteResource from './containers/delete-resource';
import deleteExpense from './containers/delete-expense';
import editUser from './containers/edit-user';
import editResource from './containers/edit-resource';
import editExpense from './containers/edit-expense';
import expenseType from './containers/expense-type';
import createExpenseType from './containers/create-epense-type';
import editExpenseType from './containers/edit-expense-type';
import deleteExpenseType from './containers/delete-expense-type';
import resourceType from './containers/resource-type';
import createResourceType from './containers/create-resource-type';
import editResourceType from './containers/edit-resource-type';
import deleteResourceType from './containers/delete-resource-type';
import seachUser from './containers/seach-user';
import searchResource from './containers/search-resource';
import searchExpense from './containers/search-expense';
import searchResourceType from './containers/search-resourcetype';
import searchExpenseType from './containers/search-expensetype';
ReactDOM.render(
  <Router>
      <div>
        <Route exact path='/' component={Login} />
        <Route path='/login' component={Login} />
        <Route path='/dashboard' component={dashboard} />
        <Route path='/userslist' component={usersList} />
        <Route path='/resourcelist' component={resourceList} />
        <Route path='/expenselist' component={expenseList} />
        <Route path='/createUser' component={createUser} />
        <Route path='/createResource' component={createResource} />
        <Route path='/createExpense' component={createExpense} />
        <Route path='/deleteUser/:id' component={deleteUser} />
        <Route path='/deleteResource/:id' component={deleteResource} />
        <Route path='/deleteExpense/:id' component={deleteExpense} />
        <Route path='/editUser/:id' component={editUser} />
        <Route path='/editResource/:id' component={editResource} />
        <Route path='/editExpense/:id' component={editExpense} />
        <Route path='/expensetype' component={expenseType} />
        <Route path='/createExpenseType' component={createExpenseType} />
        <Route path='/editExpenseType/:id' component={editExpenseType} />
        <Route path='/deleteExpenseType/:id' component={deleteExpenseType} />
        <Route path='/resourcetype' component={resourceType} />
        <Route path='/createResourceType' component={createResourceType} />
        <Route path='/editResourceType/:id' component={editResourceType} />
        <Route path='/deleteResourceType/:id' component={deleteResourceType} />
        <Route path='/seachUser' component={seachUser} />
        <Route path='/searchResource' component={searchResource} />
        <Route path='/searchExpense' component={searchExpense} />
        <Route path='/searchResourceType' component={searchResourceType} />
        <Route path='/searchExpenseType' component={searchExpenseType} />
      </div>
  </Router>,
  document.getElementById('root')
);
