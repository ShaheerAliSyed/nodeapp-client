import React, { Fragment } from 'react';
import SearchResourceType from '../components/search-resourcetype'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class SearchResourceTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <SearchResourceType />
        <Footer />
      </Fragment>
    );
  }
}

export default SearchResourceTypeContainer;