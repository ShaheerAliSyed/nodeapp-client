import React, { Fragment } from 'react';
import CreateExpense from '../components/create-expense'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class CreateExpenseContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <CreateExpense history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default CreateExpenseContainer;