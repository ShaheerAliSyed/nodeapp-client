import React, { Fragment } from 'react';
import SearchExpenseType from '../components/search-expensetype'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class SearchExpenseTypeTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <SearchExpenseType />
        <Footer />
      </Fragment>
    );
  }
}

export default SearchExpenseTypeTypeContainer;