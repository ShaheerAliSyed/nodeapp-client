import React, { Fragment } from 'react';
import ExpenseList from '../components/expense-list'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class resourceListContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <ExpenseList />
        <Footer />
      </Fragment>
    );
  }
}

export default resourceListContainer;