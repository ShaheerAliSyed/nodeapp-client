import React, { Fragment } from 'react';
import UsersList from '../components/users-list'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class usersListContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <UsersList />
        <Footer />
      </Fragment>
    );
  }
}

export default usersListContainer;