import React, { Fragment } from 'react';
import SearchResource from '../components/search-resource'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class SearchResourcerContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <SearchResource />
        <Footer />
      </Fragment>
    );
  }
}

export default SearchResourcerContainer;