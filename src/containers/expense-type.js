import React, { Fragment } from 'react';
import ExpenseType from '../components/expense-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class expenseTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <ExpenseType />
        <Footer />
      </Fragment>
    );
  }
}

export default expenseTypeContainer;