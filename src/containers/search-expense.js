import React, { Fragment } from 'react';
import SearchExpense from '../components/search-expense'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class SearchExpenseContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <SearchExpense />
        <Footer />
      </Fragment>
    );
  }
}

export default SearchExpenseContainer;