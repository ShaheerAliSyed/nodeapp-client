import React, { Fragment } from 'react';
import DeleteResource from '../components/delete-resource'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class DeleteResourceContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <DeleteResource resourceId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default DeleteResourceContainer;