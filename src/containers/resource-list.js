import React, { Fragment } from 'react';
import ResourceList from '../components/resource-list'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class resourceListContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <ResourceList />
        <Footer />
      </Fragment>
    );
  }
}

export default resourceListContainer;