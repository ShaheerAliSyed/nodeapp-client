import React, { Fragment } from 'react';
import DeleteResourceType from '../components/delete-resource-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class DeleteResourceTypeContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <DeleteResourceType resourcetypeId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default DeleteResourceTypeContainer;