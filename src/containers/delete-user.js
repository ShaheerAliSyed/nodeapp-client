import React, { Fragment } from 'react';
import DeleteUser from '../components/delete-user'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class DeleteUserContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <DeleteUser userId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default DeleteUserContainer;