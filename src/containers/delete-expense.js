import React, { Fragment } from 'react';
import DeleteExpense from '../components/delete-expense'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class DeleteExpenseContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <DeleteExpense expenseId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default DeleteExpenseContainer;