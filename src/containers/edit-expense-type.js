import React, { Fragment } from 'react';
import EditExpenseType from '../components/edit-expense-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class EditExpenseTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <EditExpenseType expensetypeId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default EditExpenseTypeContainer;