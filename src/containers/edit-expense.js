import React, { Fragment } from 'react';
import EditExpense from '../components/edit-expense'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class EditExpenseContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <EditExpense expenseId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default EditExpenseContainer;