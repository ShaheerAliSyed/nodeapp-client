import React, { Fragment } from 'react';
import CreateResource from '../components/create-resource'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class CreateResourceContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <CreateResource history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default CreateResourceContainer;