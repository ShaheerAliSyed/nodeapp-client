import React, { Fragment } from 'react';
import DeleteExpenseType from '../components/delete-expense-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class DeleteExpenseTypeContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <DeleteExpenseType expensetypeId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default DeleteExpenseTypeContainer;