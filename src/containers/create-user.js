import React, { Fragment } from 'react';
import CreateUser from '../components/create-user'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class CreateUserContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <CreateUser history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default CreateUserContainer;