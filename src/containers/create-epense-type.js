import React, { Fragment } from 'react';
import CreateExpenseType from '../components/create-expense-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class CreateExpenseTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <CreateExpenseType history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default CreateExpenseTypeContainer;