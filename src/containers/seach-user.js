import React, { Fragment } from 'react';
import SearchUser from '../components/search-user'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class SearchUserContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <SearchUser />
        <Footer />
      </Fragment>
    );
  }
}

export default SearchUserContainer;