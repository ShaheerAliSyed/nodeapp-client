import React, { Fragment } from 'react';
import EditResourceType from '../components/edit-resource-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class EditResourceTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <EditResourceType resourcetypeId={this.props.match.params.id} history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default EditResourceTypeContainer;