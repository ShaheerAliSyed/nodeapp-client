import React, { Fragment } from 'react';
import CreateResourceType from '../components/create-resource-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class CreateResourceTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <CreateResourceType history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default CreateResourceTypeContainer;