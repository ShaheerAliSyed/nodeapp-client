import React, { Fragment } from 'react';
import ResourceType from '../components/resource-type'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class ResourceTypeContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <ResourceType />
        <Footer />
      </Fragment>
    );
  }
}

export default ResourceTypeContainer;