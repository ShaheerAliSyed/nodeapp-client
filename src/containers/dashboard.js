import React, { Fragment } from 'react';
import Dashboard from '../components/dashboard'
import Navbar from '../components/nav-bar'
import SideBar from '../components/side-bar'
import Footer from '../components/footer'
class DashboardContainer extends React.Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <SideBar />
        <Dashboard history={this.props.history}/>
        <Footer />
      </Fragment>
    );
  }
}

export default DashboardContainer;