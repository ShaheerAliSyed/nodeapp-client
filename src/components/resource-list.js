import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
class resourceList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resource: []
  };
  }
  componentDidMount() {
    axios.get(`http://localhost:3300/resource`)
      .then(res => {
        const resource = res.data.resource;
        console.log(resource);

          this.setState({
            resource
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  render() {
    return (

      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>All Resources</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
        {this.state.resource.map((res) => (
          <div className="card">
         
            <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Amount</th>
                  <th>User Name</th>
                  <th>Resource Type</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <th>Action</th>
                </tr>
                </thead>
                
                <tbody>
                <tr>
                  <td>{res.id}</td>
                  <td>{res.title}</td>
                  <td>{res.content}</td>
                  <td>{res.amount}</td>
                  <td>{res.user.name}</td>
                  <td>{res.resourceType.title}</td>
                  <td>{res.createdAt}</td>
                  <td>{res.updatedAt}</td>
                  <td>
                  <div className="btn-group btn-group-sm" role="group" aria-label="...">
                      <button style={{background: 'lawngreen',marginRight: '11px',fontSize: "16px"}} className="btn btn-sm"><a href={`/editResource/${res.id}`}>
                        <i className="fas fa-pencil-alt" style={{color: 'black'}}/></a></button>
                      <button style={{fontSize: "16px"}} style={{color: "black"}} className="btn  btn-danger btn-sm"><a href={`/deleteResource/${res.id}`}>
                        <i className="fas fa-trash-alt" style={{color: 'white'}}/></a>
                      </button>
                  </div>
                  </td>
                </tr>
                </tbody>    
              </table>
              
           
            </div>
          </div>
             ))}
            </div>
          </div>
    </section>
      </div>
    );
  }
}

export default resourceList;