import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import Table from './table';
class expenseType extends Component {
  
    state = {
        expenseTypes: []
    };
  
  
  componentDidMount() {
    axios.get(`http://localhost:3300/expenseType`)
      .then(res => {
        const expenseTypes = res.data.expensetype;
        console.log(expenseTypes);

          this.setState({
            expenseTypes
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  render() {
    return (

      
      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>All Expense Types</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
        <Table data={this.state.expenseTypes} ></Table>
            </div>
          </div>
    </section>
      </div>
    );
  }
}

export default expenseType;