import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class viewResource extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      resources: []
    };
}
    
  componentDidMount() {
    const id = this.props.match.params.id 
    axios.get(`http://localhost:3300/resource/${id}`)
      .then(res => {
        const resources = res.data.resource;
        console.log(resources);

          this.setState({
            resources
          })
      })
      .catch((error) => {
        console.log(error)
    })

    axios.get(`http://localhost:3300/users/${id}`)
      .then(res => {
        const users = res.data.user;
        console.log(users);

          this.setState({
            users
          })
      })
      .catch((error) => {
        console.log(error)
    })

  }
  render() {
    console.log(this.state.resources,'jjijii');
    console.log(this.state.users,'jjijii');
    return (

      <div classNameName="container">
        <nav className="navbar">
        <h1 className="firstheading">List of Resource</h1>
        <Button variant="info" className="backtdashboard"><Link to="/dashboard">Home</Link></Button>
        <Button variant="info" className="create"><Link to="/resourcelist">All Resources</Link></Button>
        </nav>
        <div classNameName="col-xs-8 users-displaybox">
          <div classNameName="card">
           <div classNameName="card-body">
                <h4 classNameName="card-title"value="userId">Resource ID = {this.state.resources.id}</h4>
               <h5 classNameName="card-title">Resource title = {this.state.resources.title}</h5>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              Resource content = {this.state.resources.content}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              Resource amount = {this.state.resources.amount}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              User Name = {this.state.users.name}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Created At = {this.state.resources.createdAt}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Updated At = {this.state.resources.updatedAt}             
              </h6>
            </div>
          </div>
        </div>
       </div>
    );
  }
}

export default viewResource;