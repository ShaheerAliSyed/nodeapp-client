import React from 'react';
import { Link } from 'react-router-dom';
class Dashboard extends React.Component  {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
}

 

  render() {
  return (
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
    <ul className="navbar-nav">
      <li className="nav-item">
        <a className="nav-link" data-widget="pushmenu" href="#"><i className="fas fa-bars"></i></a>
      </li>    
      <li><button value="Logout" className="btn btn-light logout-btn" style={{marginLeft: "933px"}}>
        <Link to="/login">Log Out</Link></button></li>
    </ul>
  </nav>
   );
  }
}

export default Dashboard;