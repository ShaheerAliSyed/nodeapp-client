import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class viewResourceType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resourceTypes: []
  };
}
   
  componentDidMount() {
    const id = this.props.match.params.id 
    axios.get(`http://localhost:3300/resourceType/${id}`)
      .then(res => {
        console.log(res);
        const resourceTypes = res.data.resourcetype;
        console.log(resourceTypes);

          this.setState({
            resourceTypes
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  render() {
    console.log(this.state.resourceTypes,'jjijii');
    return (

      <div classNameName="container">
        <nav className="navbar">
        <h1 className="firstheading">List of Resource Types</h1>
        <Button variant="info" className="backtdashboard"><Link to="/dashboard">Home</Link></Button>
        <Button variant="info" className="create"><Link to="/resourcetype">All Resource Types</Link></Button>
        </nav>
        <div classNameName="col-xs-8 users-displaybox">
          <div classNameName="card">
           <div classNameName="card-body">
                <h4 classNameName="card-title"value="userId">Expense Type ID = {this.state.resourceTypes.id}</h4>
               <h5 classNameName="card-title">Expense Type title = {this.state.resourceTypes.title}</h5>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              Expense Type Description = {this.state.resourceTypes.description}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Created At = {this.state.resourceTypes.createdAt}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Updated At = {this.state.resourceTypes.updatedAt}             
              </h6>
            </div>
          </div>
        </div>
       </div>
    );
  }
}

export default viewResourceType;