import React, { Component } from 'react';
import axios from 'axios'
// import './login.css';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
    this.onChange = this.onChange.bind(this)
    this.submitForm = this.submitForm.bind(this)
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value},()=>{
      console.log(this.state);
    }); 
  }
  submitForm = (e) => {
    e.preventDefault();

    const admin = {
      email: this.state.email,
      password: this.state.password
    }
    console.log(admin);
    
    axios.post(`http://localhost:3300/login`, admin)
    .then(res => {
      console.log(res.data);
      this.props.history.push('/dashboard');
    }).catch(error => {
      console.log(error);
    });
  };

  render() {
    return (
<div className="hold-transition login-page"> 
<div className="login-box">
  <div className="login-logo">
    <b>Welcome Admin</b>
  </div>
  <div className="card">
    <div className="card-body login-card-body">
      <p className="login-box-msg">Please Sign in</p>

      <form className="form-signin" onSubmit={this.submitForm}>
        <div className="input-group mb-3">
          <input type="email" name = "email" value={this.state.email} onChange={this.onChange} 
          className="form-control" placeholder="Email"/>
          <div className="input-group-append">
            <div className="input-group-text">
              <span className="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div className="input-group mb-3">
          <input type="password" name = "password" value={this.state.password} 
          onChange={this.onChange} className="form-control" placeholder="Password"/>
          <div className="input-group-append">
            <div className="input-group-text">
              <span className="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-8">
            <div className="icheck-primary">
              <input type="checkbox" id="remember"/>
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          
          <div className="col-4">
            <button type="submit" className="btn btn-primary btn-block">Sign In</button>
          </div>
      
        </div>
      </form>
      </div>
      </div>
      </div>
      </div>
    );
  }
}

export default Login;