import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      resources: [],
      expenses: [],
      resourcetypes: [],
      expensetypes: []
    };
  }

  componentDidMount() {
    axios.get(`http://localhost:3300/users`)
      .then(res => {
        const users = res.data.user;


        this.setState({
          users
        })
        console.log(users);
      })
      .catch((error) => {
        console.log(error)
      });

      axios.get(`http://localhost:3300/resource`)
      .then(res => {
        const resources = res.data.resource;


        this.setState({
          resources
        })
        console.log(resources);
      })
      .catch((error) => {
        console.log(error)
      });

      axios.get(`http://localhost:3300/expense`)
      .then(res => {
        const expenses = res.data.expense;


        this.setState({
          expenses
        })
        console.log(expenses);
      })
      .catch((error) => {
        console.log(error)
      });

      axios.get(`http://localhost:3300/resourceType`)
      .then(res => {
        const resourcetypes = res.data.resourcetype;


        this.setState({
          resourcetypes
        })
        console.log(resourcetypes);
      })
      .catch((error) => {
        console.log(error)
      });

      axios.get(`http://localhost:3300/expenseType`)
      .then(res => {
        const expensetypes = res.data.expensetype;


        this.setState({
          expensetypes
        })
        console.log(expensetypes);
      })
      .catch((error) => {
        console.log(error)
      });
  }
  countUsers() {
    const countUsers = this.state.users;
    return countUsers.length;
  }
  countResources() {
    const countResources = this.state.resources;
    return countResources.length;
  }
  countExpenses() {
    const countExpenses = this.state.expenses;
    return countExpenses.length;
  }
  countResourcetypes() {
    const countResourcetypes = this.state.resourcetypes;
    return countResourcetypes.length;
  }
  countExpenseTypes() {
    const countExpenseTypes = this.state.expensetypes;
    return countExpenseTypes.length;
  }
  render() {
    return (
       

        <div className="content-wrapper">
        <div className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1 className="m-0 text-dark">Dashboard</h1>
          </div>
        </div>
      </div>
    </div>
        <section className="content">
          <div className="container-fluid">
            <div className="row">
            <div className="col-lg-3 col-6">
                <div className="small-box bg-warning">
                  <div className="inner">
                    <h3>{this.countUsers()}</h3>

                    <p>User</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-person-add"></i>
                  </div>
                  <Link to="/seachUser" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-info">
                  <div className="inner">
                    <h3>{this.countResources()}</h3>

                    <p>Resources</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-bag"></i>
                  </div>
                  <Link to="/searchResource" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-success">
                  <div className="inner">
                    <h3>{this.countExpenses()}</h3>

                    <p>Expenses</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-stats-bars"></i>
                  </div>
                  <Link to="/searchExpense" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-danger">
                  <div className="inner">
                    <h3>{this.countResourcetypes()}</h3>

                    <p>Resource Type</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-pie-graph"></i>
                  </div>
                  <Link to="/searchResourceType" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-danger">
                  <div className="inner">
                    <h3>{this.countExpenseTypes()}</h3>

                    <p>Expense Type</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-pie-graph"></i>
                  </div>
                  <Link to="/searchExpenseType" className="small-box-footer">More info <i className="fas fa-arrow-circle-right"></i></Link>
                </div>
              </div>
            </div>
          </div>
        </section>
        </div>
    

    );
  }
}

export default Dashboard;