import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class viewUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: []
    };
  }
  componentDidMount() {
    const id = this.props.match.params.id 
    axios.get(`http://localhost:3300/users/${id}`)
      .then(res => {
        const users = res.data.user;
        console.log(users);

          this.setState({
            users
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  render() {
    return (

      <div classNameName="container">
        <nav className="navbar">
        <h1 className="firstheading">List of all users</h1>
        <Button variant="info" className="backtdashboard"><Link to="/dashboard">Home</Link></Button>
        <Button variant="info" className="create"><Link to="/userslist">All User</Link></Button>
        </nav>
        <div classNameName="col-xs-8 users-displaybox">
          <div classNameName="card">
           <div classNameName="card-body">
                <h4 classNameName="card-title"value="userId">User ID = {this.state.users.id}</h4>
               <h5 classNameName="card-title">User Name = {this.state.users.name}</h5>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                User Email = {this.state.users.email}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                User Password = {this.state.users.password}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Created At = {this.state.users.createdAt}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Updated At = {this.state.users.updatedAt}             
              </h6>
              <Button variant="primary" className="delete">Delete</Button>
            </div>
          </div>
        </div>
       </div>
    );
  }
}

export default viewUser;