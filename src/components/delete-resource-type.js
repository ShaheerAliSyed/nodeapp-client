import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class deleteResource extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resourceTypes: []
    };
} 
  componentDidMount() {
    const id = this.props.resourcetypeId
    axios.get(`http://localhost:3300/resourceType/${id}`)
      .then(res => {
        const resourceTypes = res.data.resourcetype;
        console.log(resourceTypes);

          this.setState({
            resourceTypes
          })
      })
      .catch((error) => {
        console.log(error)
    }) 
  };
  handleSubmit = event => {
    if (window.confirm("Do you really want to delete?")) {
    event.preventDefault();
    const id = this.props.resourcetypeId
    axios.delete(`http://localhost:3300/resourceType/${id}`)
      .then(res => {
        this.props.history.push('/searchResourceType');
      });
    }
    }
  render() {
    return (
<div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Delete Resource Type</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
  
          <div className="card">
         
            <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Resource Type ID</th>
                  <th>Resource Type Title</th>
                  <th>Resource Type description</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                </tr>
                </thead>
                
                <tbody>
                <tr>
                  <td>{this.state.resourceTypes.id}</td>
                  <td>{this.state.resourceTypes.title}</td>
                  <td>{this.state.resourceTypes.description}</td>
                  <td>{this.state.resourceTypes.createdAt}</td>
                  <td>{this.state.resourceTypes.updatedAt}</td>
                </tr>
                </tbody>   
                
              </table>
              <Button variant="primary" className="delete" onClick={this.handleSubmit} style={{marginTop: '17px'}}>
                  Delete
                  </Button> 
               
            </div>
          </div>
            </div>
          </div>
    </section>
      </div>
      
      
    );
  }
}

export default deleteResource;