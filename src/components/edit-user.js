import React, { Component } from 'react';
import axios from 'axios'

class editUser extends Component {
    constructor(props) {
        super(props);
    this.state = {
        name: '',
        email: '',
        password: ''
    };
    
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
};
    handleChange = (e) => {
        this.setState({ 
            [e.target.name]: e.target.value},()=>{
          }); 
      }
      componentDidMount() {
    const id = this.props.userId
    axios.get(`http://localhost:3300/users/${id}`)
          .then(res => {
            const users = res.data.user;
            console.log(users);
    
              this.setState({
                users
              })
          })
          .catch((error) => {
            console.log(error)
        }) 
      };
      handleCancel = event => {
        this.props.history.push('/seachUser');
      }
  handleSubmit = event => {
    event.preventDefault();
    const user = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      };
      const id = this.props.userId
    axios.put(`http://localhost:3300/users/${id}`, user )
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/seachUser');
      });
    }
  render() {
    
    return (

      <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>User Updation Form</h1>
            </div>
          </div>
          </div>
      </section>
        <section className="content">
          <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12">
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Edit User</h3>
  
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i className="fas fa-minus"></i></button>
                </div>
              </div>
              <div className="card-body">
                <form onSubmit={this.handleSubmit} className="usercreationform">
                  <div className="row">

                  <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputName">User Name</label>
                  <input type="text" name="name" placeholder="Username"value={this.state.name}
                  onChange={this.handleChange} className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail">User Email:</label>
                  <input  type="text"  name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange}
                  className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail">User Password:</label>
                  <input  type="password"  name="password" placeholder="Password" value={this.state.password} 
                  onChange={this.handleChange}
                  className="form-control"/>
                </div>
                </div>
                
                  <div className="col-12">
                    
                    <button type="submit" className="btn btn-success float-right" style={{marginLeft: '8px'}}>Update</button>
                    <button onClick={this.handleCancel} className="btn btn-secondary float-right">Cancel</button>
                  </div>
                </div>
                </form>
              </div>       
            </div>
          </div>
        </div>
        </div>
      </section>
      </div>
    );
  }
}

export default editUser;