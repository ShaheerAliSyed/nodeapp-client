import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class EditResource extends Component {
    constructor(props) {
        super(props);
    this.state = {
        users: [],
        resourcetypes: [],
        title: '',
        content: '',
        amount: '',
        userId: '',
        resourceTypeId: ''
    };
    
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
};
    handleChange = (e) => {
        this.setState({ 
            [e.target.name]: e.target.value},()=>{
          }); 
      }
  componentDidMount() {
    const id = this.props.userId
    axios.get(`http://localhost:3300/resource/${id}`)
      .then(res => {
        const resources = res.data.resource;
        console.log(resources);

          this.setState({
            resources
          })
      })
      .catch((error) => {
        console.log(error)
    })

    axios
     .get('http://localhost:3300/users')
     .then(response => { 
      const users = response.data.user;
          this.setState({
            users
          });
          console.log(users)
     })
     .catch(error => console.log(error.response));

     axios
     .get('http://localhost:3300/resourceType')
     .then(response => { 
      const resourcetypes = response.data.resourcetype;
          this.setState({
            resourcetypes
          });
          console.log(resourcetypes)
     })
     .catch(error => console.log(error.response));
  }
  handleCancel = event => {
    this.props.history.push('/searchResource');
  }
  handleSubmit = event => {
    event.preventDefault();
    const resource = {
        title: this.state.title,
        content: this.state.content,
        amount: this.state.amount,
        userId: this.state.userId,
        resourceTypeId: this.state.resourceTypeId
      };
      const id = this.props.userId
    axios.put(`http://localhost:3300/resource/${id}`, resource )
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/searchResource');
      });
    }
  render() {
    // console.log(this.state.users,'jjijii');
    return (

      <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Edit Resource</h1>
            </div>
          </div>
          </div>
      </section>
        <section className="content">
          <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12">
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Edit Resource</h3>
  
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i className="fas fa-minus"></i></button>
                </div>
              </div>
              <div className="card-body">
                <form onSubmit={this.handleSubmit} className="usercreationform">
                  <div className="row">

                  <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputName">Resource Title:</label>
                  <input type="text" name="title" placeholder="Title" value={this.state.title}
                  onChange={this.handleChange} className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail"> Resource Content:</label>
                  <input  type="text"  name="content" placeholder="Content" value={this.state.content} onChange={this.handleChange}
                  className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail">Resource Amount:</label>
                  <input  type="number"  name="amount" placeholder="Amount" value={this.state.amount} 
                  onChange={this.handleChange}
                  className="form-control"/>
                  </div>
                  </div>
                  <div className="col-lg-4">
                  <div className="form-group">
                  <label>
                  Select Resource Type:
                  </label>
                  <select className="form-control" name="resourceTypeId" onChange={this.handleChange}>
                    <option>Click Here</option>
                    {this.state.resourcetypes.map(resourcetype => (
                      <option  key={resourcetype.id} value={resourcetype.id}>
                        {resourcetype.title}
                      </option>
                    ))}
                  </select>
                
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                <label>
                  Select User
                  </label>
                  <select className="form-control" name="userId" onChange={this.handleChange}>
                  <option>Click Here</option>
                    {this.state.users.map(user => (
                      <option  key={user.id} value={user.id}>
                        {user.name}
                      </option>
                    ))}
                  </select>
                
                </div>
                </div>
               
                  <div className="col-lg-12">
                    
                    <button type="submit" className="btn btn-success float-right" style={{marginLeft: '8px'}}>Update</button>
                    <button onClick={this.handleCancel} className="btn btn-secondary float-right">Cancel</button>
                  </div>
                </div>
            
                </form>
              </div>       
            </div>
          </div>
        </div>
        </div>
      </section>
      </div>
    );
  }
}

export default EditResource;