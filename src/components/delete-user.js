import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class DeleteUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
    this.handleSubmit = this.handleSubmit.bind(this)
  }

    
  componentDidMount() {
    
    const id = this.props.userId
    axios.get(`http://localhost:3300/users/${id}`)
      .then(res => {
        const users = res.data.user;
        console.log(users);

          this.setState({
            users
          })
      })
      .catch((error) => {
        console.log(error)
    }) 
  };
  handleSubmit = event => {
    if (window.confirm("Do you really want to delete?")) {
    event.preventDefault();
    const id = this.props.userId
    axios.delete(`http://localhost:3300/users/${id}`)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/seachUser');
      });
    }
    }
  render() {
    console.log(this.state.users,'jjijii');
    return (
      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Delete User</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
  
          <div className="card">
         
            <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>User Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                </tr>
                </thead>
                
                <tbody>
                <tr>
                  <td>{this.state.users.id}</td>
                  <td>{this.state.users.name}</td>
                  <td>{this.state.users.email}</td>
                  <td>{this.state.users.password}</td>
                  <td>{this.state.users.createdAt}</td>
                  <td>{this.state.users.updatedAt}</td>
                </tr>
                </tbody>   
                
              </table>
              <Button variant="primary" className="delete" onClick={this.handleSubmit} style={{marginTop: '17px'}}>
                  Delete
                  </Button> 
               
            </div>
          </div>
            </div>
          </div>
    </section>
      </div>
      
    );
  }
}

export default DeleteUser;