import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';;
class expenseList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expense: []
  };
}
  
  
  
  componentDidMount() {
    axios.get(`http://localhost:3300/expense`)
      .then(res => {
        const expense = res.data.expense;
        console.log(expense);

          this.setState({
            expense
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  render() {
    return (
      <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>All Expenses</h1>
            </div>
          </div>
        </div>
      </section>
      <section className="content">
        <div className="row">
          <div className="col-12">
          {this.state.expense.map((exp) => (
            <div className="card">
           
              <div className="card-body">
                <table id="example2" className="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Amount</th>
                    <th>User Name</th>
                    <th>Expense Type</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  
                  <tbody>
                  <tr>
                    <td>{exp.id}</td>
                    <td>{exp.title}</td>
                    <td>{exp.amount}</td>
                    <td>{exp.user.name}</td>
                    <td>{exp.expenseType.title}</td>
                    <td>{exp.createdAt}</td>
                    <td>{exp.updatedAt}</td>
                    <td>
                    <div className="btn-group btn-group-sm" role="group" aria-label="...">
                        <button style={{background: 'lawngreen',marginRight: '11px',fontSize: "16px"}} className="btn btn-sm"><a href={`/editExpense/${exp.id}`}>
                          <i className="fas fa-pencil-alt" style={{color: 'black'}}/></a></button>
                        <button style={{fontSize: "16px"}} style={{color: "black"}} className="btn  btn-danger btn-sm"><a href={`/deleteExpense/${exp.id}`}>
                          <i className="fas fa-trash-alt" style={{color: 'white'}}/></a>
                        </button>
                    </div>
                    </td>
                  </tr>
                  </tbody>    
                </table>
                
             
              </div>
            </div>
               ))}
              </div>
            </div>
      </section>
        </div>
      
    );
  }
}

export default expenseList;