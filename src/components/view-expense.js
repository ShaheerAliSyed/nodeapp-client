import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class viewExpense extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      resources: [],
      expenseTypes: [],
      expenses: []
    };
}
    
  componentDidMount() {
    const id = this.props.match.params.id 
    axios.get(`http://localhost:3300/expense/${id}`)
      .then(res => {
        const expenses = res.data.expense;
        console.log(expenses);

          this.setState({
            expenses
          })
      })
      .catch((error) => {
        console.log(error)
    })

    axios.get(`http://localhost:3300/users/${id}`)
      .then(res => {
        const users = res.data.user;
        console.log(users);

          this.setState({
            users
          })
      })
      .catch((error) => {
        console.log(error)
    })

    axios.get(`http://localhost:3300/resource/${id}`)
      .then(res => {
        const resources = res.data.resource;
        console.log(resources);

          this.setState({
            resources
          })
      })
      .catch((error) => {
        console.log(error)
    })

    axios.get(`http://localhost:3300/expenseType/${id}`)
      .then(res => {
        const expenseTypes = res.data.expensetype;
        console.log(expenseTypes);

          this.setState({
            expenseTypes
          })
      })
      .catch((error) => {
        console.log(error)
    })

  }
  render() {
    console.log(this.state.users,'jjijii');
    console.log(this.state.resources,'jjijii');
    console.log(this.state.expenseTypes,'jjijii');
    return (

      <div classNameName="container">
        <nav className="navbar">
        <h1 className="firstheading">List of Expenses</h1>
        <Button variant="info" className="backtdashboard"><Link to="/dashboard">Home</Link></Button>
        <Button variant="info" className="create"><Link to="/expenselist">All Expenses</Link></Button>
        </nav>
        <div classNameName="col-xs-8 users-displaybox">
          <div classNameName="card">
           <div classNameName="card-body">
                <h4 classNameName="card-title"value="userId">Expense ID = {this.state.expenses.id}</h4>
               <h5 classNameName="card-title">Expense title = {this.state.expenses.title}</h5>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              Expense amount = {this.state.expenses.amount}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              User Name = {this.state.users.name}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              Resource Title = {this.state.resources.title}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
              Expense Type = {this.state.expenseTypes.title}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Created At = {this.state.expenses.createdAt}             
              </h6>
              <h6 classNameName="card-subtitle mb-2 text-muted">
                Updated At = {this.state.expenses.updatedAt}             
              </h6>
            </div>
          </div>
        </div>
       </div>
    );
  }
}

export default viewExpense;