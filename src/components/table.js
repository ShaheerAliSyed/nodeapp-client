import React from 'react';
import { Table } from 'reactstrap';
const Row = (props) => {
    return (
        props.keys && props.keys.map((key) =>
            <Col value={props.obj[key]}></Col>
        ) || <td>None</td>

    )
}
const Col = (props) => {
    return (
        <td>{(props.value)}</td>
    ) || <td>None</td>
}
class table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    componentDidUpdate(prevProps) {
        if (this.props.data.length !== prevProps.data.length) {
            var keys = Object.keys(this.props.data[0])
            this.setState({
                keys
            })
        }
    }

    render() {
        console.log(this.state.values, 'values');
        return (
        
            <Table>
                <div className="card">

                    <div className="card-body">
                        <table id="example2" className="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    {
                                        this.state.keys && this.state.keys.map((key, index) =>
                                            <th key={index}>{key}</th>
                                        )
                                    }
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.data && this.props.data.map((user) =>
                                        <tr>
                                            <Row keys={this.state.keys} obj={user}></Row>
                                            <td><div className="btn-group btn-group-sm" role="group" aria-label="...">
                      <button style={{background: 'lawngreen',marginRight: '11px',fontSize: "16px"}} className="btn btn-sm"><a href={`/editUser/${user.id}`}>
                        <i className="fas fa-pencil-alt" style={{color: 'black'}}/></a></button>
                      <button style={{fontSize: "16px"}} style={{color: "black"}} className="btn  btn-danger btn-sm"><a href={`/deleteUser/${user.id}`}>
                        <i className="fas fa-trash-alt" style={{color: 'white'}}/></a>
                      </button>
                  </div></td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </Table>
       

        );
    }
}
export default table;