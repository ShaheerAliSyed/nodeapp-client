import React from 'react';
import axios from 'axios';
import './login.css';
class createUser extends React.Component {
    constructor(props) {
        super(props);
    this.state = {
    name: '',
    email: '',
    password: ''
  };
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    }
  handleChange = (e) => {
    this.setState({ 
        [e.target.name]: e.target.value},()=>{
        console.log(this.state);
      }); 
  }
  handleCancel = event => {
    this.props.history.push('/seachUser');
  }
  handleSubmit = event => {
    event.preventDefault();

    const user = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };
    // console.log(user);
    
    axios.post(`http://localhost:3300/users`,  user )
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/seachUser');
      }).catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Users Registertion Form</h1>
          </div>
        </div>
        </div>
    </section>
      <section className="content">
        <div className="container-fluid">
      <div className="row">
        <div className="col-lg-12">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Add User</h3>

              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i className="fas fa-minus"></i></button>
              </div>
            </div>
            <div className="card-body">
              <form onSubmit={this.handleSubmit} className="usercreationform" style={{display: 'flex'}}>
                <div className="row">
                  <div className="col-lg-4">
              <div className="form-group" style={{marginRight: '10px'}}>
                <label for="inputName">User Name</label>
                <input type="text" name="name" placeholder="username"value={this.state.name}
                onChange={this.handleChange} className="form-control"/>
              </div>
              </div>
              <div className="col-lg-4">
              <div className="form-group" style={{marginRight: '10px'}}>
                <label for="inputEmail">User Email:</label>
                <input  type="text"  name="email" placeholder="email" value={this.state.email} onChange={this.handleChange}
                className="form-control"/>
              </div>
              </div>
              <div className="col-lg-4">
              <div className="form-group" style={{marginRight: '10px'}}>
                <label for="inputEmail">User Password:</label>
                <input  type="password"  name="password" placeholder="Password" value={this.state.password} 
                onChange={this.handleChange}
                className="form-control"/>
              </div>
              </div>
                <div className="col-lg-12">
                <button type="submit" className="btn btn-success float-right" style={{marginLeft: '8px'}}>Submit</button>
                  <button onClick={this.handleCancel} className="btn btn-secondary float-right">Cancel</button>
                </div>
              </div>
              </form>
            </div>       
          </div>
        </div>
      </div>
      </div>
    </section>
    </div>
    )
  }
}
export default createUser;