import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Moment from 'react-moment';
import 'moment-timezone';
import axios from 'axios'
import './login.css';
class Search extends Component {
    state = {
      search: '',
      users: []
    }
    componentDidMount = () => {
        axios.get(`http://localhost:3300/users`)
          .then( res  => {
            const users = res.data.user;
            console.log(users);
            this.setState({
              users
              
            })
          })
      }
      updateSearch(event){
        this.setState({
          search: event.target.value.substr(0,20)
          
        });
        
    }
    render() {
      console.log(this.state.search)
      console.log(this.state.users, 'ssddsds')
        let filteredUsers = this.state.users.filter(
          (user) => {
            return user.email.indexOf(this.state.search) !== -1;
          }
          
        );
        console.log(filteredUsers, 'ssd')
      return (
        <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="pageheading">All Users</h1>
              </div>
            </div>
          </div>
        </section>
        <div>
          <section className="content">
          <div className="inputSearch" 
        style={{ padding:"5px", width:"255px", marginBottom:"20px", }} 
         >
        <input className="form-control" name="text" type="text" placeholder="Search User by Name"
        onChange={this.updateSearch.bind(this)}
        value={this.state.search} />
        <div>
        {filteredUsers.map((user) => {
          return <div user={user} key={user.id}/>
        })}
        </div>
        </div>
      <div className="row">
        <div className="col-12">
          <div className="card">

          <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                  <thead>
                      <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Password</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      {filteredUsers.map(user => (
                <tr>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.password}</td>
                <td><Moment format="YYYY/MM/DD">{user.createdAt}</Moment></td>
                <td><Moment format="YYYY/MM/DD">{user.updatedAt}</Moment></td>
                <td><div className="btn-group btn-group-sm" role="group" aria-label="...">
                      <Button style={{marginRight: '11px',fontSize: "16px"}} className="btn btn-success btn-sm"><Link to={`/editUser/${user.id}`}>
                        <i className="fas fa-pencil-alt" style={{color: 'white'}}/></Link></Button>
                      <Button style={{fontSize: "16px"}} style={{color: "black"}} className="btn  btn-danger btn-sm"><Link to={`/deleteUser/${user.id}`}>
                        <i className="fas fa-trash-alt" style={{color: 'white'}}/></Link>
                      </Button>
                  </div></td>
                  </tr>
                  ))}
                  </tbody>
          </table>
          </div>
          </div>
       
          </div>
          </div>
          </section>
        </div>
        
        </div>
      )
    }
   }
   
   export default Search