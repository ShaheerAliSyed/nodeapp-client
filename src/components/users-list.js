import React, { Component } from 'react';
import Table from './table';
import axios from 'axios';
import './login.css';
class usersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialUsers: [],
      users: []
    };
}
  filerList = (event) => {
    let items = this.state.initialUsers;
    items = items.filter((item) => {
      return item.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
    });
    this.setState({items: items})
  }
  componentWillMount = () => {
    this.setState({
      initialUsers: this.state.users
    })
  }
  componentDidMount() {
    axios.get(`http://localhost:3300/users`)
      .then(res => {
        const users = res.data.user;
        console.log(users);

          this.setState({
            users
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  
  render() {
    return (
      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>All Users</h1>
          </div>
          <div>
            <form>
              <input type="text" placeholder="Search User" onChange={this.filerList}></input>
            </form>
            <div>
              {
                this.state.users.map(function(user){
                  return <div key={user}>{user.name}</div>
                })
              }
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
          <Table data={this.state.users} ></Table>
            </div>
          </div>
    </section>
      </div>
    );
  }
}

export default usersList;