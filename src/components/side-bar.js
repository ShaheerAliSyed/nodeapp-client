import React from 'react';
import { Link } from 'react-router-dom';
class Dashboard extends React.Component  {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
}

  render() {
  return (
    <aside className="main-sidebar sidebar-dark-primary elevation-4">
    <Link to="/dashboard" className="brand-link">
      {/* <img src="#" alt="AdminLTE Logo" 
           style={{opacity: ".8"}}/> */}
      <span className="brand-text font-weight-light">Dashboard</span>
    </Link>
    <div className="sidebar">
      <div className="user-panel mt-3 pb-3 mb-3 d-flex">
        <div className="">
          {/* <img src="#" className="" alt="User Image"/> */}
        </div>
        <div className="info">
          <Link to="/dashboard" className="d-block">Admin Panel</Link>
        </div>
      </div>

      <nav className="mt-2">
        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li className="nav-item has-treeview menu-open">
            <Link to="/dashboard" className="nav-link">
              <i className="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </Link>
          </li>
          <li className="nav-item">
            <Link to="#" className="nav-link"><i className="nav-icon ion-person-add"></i><p> Users<i className="fas fa-angle-left right"></i></p>
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="/createUser" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/seachUser" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p> Search</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className="nav-item has-treeview">
            <Link to="#" className="nav-link">
              <i className="nav-icon ion-bag"></i><p>  Resources <i className="fas fa-angle-left right"></i></p>
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="/createResource" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/searchResource" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Search</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className="nav-item has-treeview">
            <Link to="#" className="nav-link">
              <i className="nav-icon ion-stats-bars"></i><p> Expenses
                <i className="fas fa-angle-left right"></i></p>
              
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="/createExpense" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/searchExpense" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Search</p>
                </Link>
              </li>
            </ul>
          </li> 
          <li className="nav-item has-treeview">
            <Link to="#" className="nav-link">
              <i className="nav-icon ion-pie-graph"></i><p> Resource Types
                <i className="fas fa-angle-left right"></i></p>
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="/createResourceType" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/searchResourceType" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Search</p>
                </Link>
              </li>
            </ul>
          </li>     
          <li className="nav-item has-treeview">
            <Link to="#" className="nav-link">
              <i className="nav-icon ion-pie-graph"></i><p> Expense Types
                <i className="fas fa-angle-left right"></i></p>
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="/createExpenseType" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/searchExpenseType" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Search</p>
                </Link>
              </li>
            </ul>
          </li>         
          <li className="nav-header">LABELS</li>
          <li className="nav-item">
            <a href="#" className="nav-link">
              <i className="nav-icon far fa-circle text-danger"></i>
              <p className="text">Important</p>
            </a>
          </li>
          <li className="nav-item">
            <a href="#" className="nav-link">
              <i className="nav-icon far fa-circle text-warning"></i>
              <p>Warning</p>
            </a>
          </li>
          <li className="nav-item">
            <a href="#" className="nav-link">
              <i className="nav-icon far fa-circle text-info"></i>
              <p>Informational</p>
            </a>
          </li>
        </ul>
      </nav>
    
    </div>
  </aside>
   );
  }
}

export default Dashboard;