import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import axios from 'axios'
import Moment from 'react-moment';
import 'moment-timezone';
import './login.css';
class Search extends Component {
    state = {
      search: '',
      resourcetypes: []
    }
    componentDidMount = () => {
        axios.get(`http://localhost:3300/resourceType`)
          .then( res  => {
            const resourcetypes = res.data.resourcetype;
            console.log(resourcetypes);
            this.setState({
                resourcetypes
              
            })
          })
      }
      updateSearch(event){
        this.setState({
          search: event.target.value.substr(0,20)
          
        });
        
    }
    render() {
      console.log(this.state.search)
      console.log(this.state.resourcetypes, 'ssddsds')
        let filteredResourceTypes = this.state.resourcetypes.filter(
          (resourcetype) => {
            return resourcetype.description.toLowerCase().indexOf(this.state.search) !== -1;
          }
          
        );
        console.log(filteredResourceTypes, 'ssd')
      return (
        <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="pageheading" style={{marginLeft:"250px", fontFamily:"-webkit-body"}}>Search Resource Types</h1>
              </div>
            </div>
          </div>
        </section>
        <div>
          <section className="content">
          <div className="inputSearch" 
        style={{ padding:"5px", width:"255px", marginBottom:"20px",}} 
         >
        <input className="form-control" name="text" type="text" placeholder="Search Resource by Title"
        onChange={this.updateSearch.bind(this)}
        value={this.state.search} />
        <div>
        {filteredResourceTypes.map((resourcetype) => {
          return <div resourcetype={resourcetype} key={resourcetype.id}/>
        })}
        </div>
        </div>
      <div className="row">
        <div className="col-12">
        
          <div className="card">

          <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                  <thead>
                      <tr>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      {filteredResourceTypes.map(resourcetype => (
                <tr>
                <td>{resourcetype.title}</td>
                <td>{resourcetype.description}</td>
                <td><Moment format="YYYY/MM/DD">{resourcetype.createdAt}</Moment></td>
                <td><Moment format="YYYY/MM/DD">{resourcetype.updatedAt}</Moment></td>
                <td><div className="btn-group btn-group-sm" role="group" aria-label="...">
                      <Button style={{marginRight: '11px',fontSize: "16px"}} className="btn  btn-success btn-sm"><Link to={`/editResourceType/${resourcetype.id}`}>
                        <i className="fas fa-pencil-alt" style={{color: 'white'}}/></Link></Button>
                      <Button style={{fontSize: "16px"}} style={{color: "black"}} className="btn  btn-danger btn-sm"><Link to={`/deleteResourceType/${resourcetype.id}`}>
                        <i className="fas fa-trash-alt" style={{color: 'white'}}/></Link>
                      </Button>
                  </div></td>
                  </tr>
                    ))}
                  </tbody>
          </table>
          </div>
          </div>
        
          </div>
          </div>
          </section>
        </div>
        
        </div>
      )
    }
   }
   
   export default Search