import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import Table from './table';
class resourceType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resourceTypes: []
      
  };
}  
  componentDidMount() {
    axios.get(`http://localhost:3300/resourceType`)
      .then(res => {
        const resourceTypes = res.data.resourcetype;
        console.log(resourceTypes);

          this.setState({
            resourceTypes
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  render() {
    return (

      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>All Resource Types</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
        <Table data={this.state.resourceTypes} ></Table>
            </div>
          </div>
    </section>
      </div>
    );
  }
}

export default resourceType;