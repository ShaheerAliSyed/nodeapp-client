import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';

class deleteResource extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resources: []
    };
}
    
  componentDidMount() {
    const id = this.props.resourceId
    axios.get(`http://localhost:3300/resource/${id}`)
      .then(res => {
        const resources = res.data.resource;
        console.log(resources);

          this.setState({
            resources
          })
      })
      .catch((error) => {
        console.log(error)
    }) 
  };
  handleSubmit = event => {
    if (window.confirm("Do you really want to delete?")) {
    event.preventDefault();
    const id = this.props.resourceId
    axios.delete(`http://localhost:3300/resource/${id}`)
      .then(res => {
        this.props.history.push('/searchResource');
      });
    }
    }
  render() {
    console.log(this.state.resources,'jjijii');
    console.log(this.state.resources,'ssasddas');
    return (

      <div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Delete Resource</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
  
          <div className="card">
         
            <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Resource ID</th>
                  <th>Resource Title</th>
                  <th>Resource Content</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                </tr>
                </thead>
                
                <tbody>
                <tr>
                  <td>{this.state.resources.id}</td>
                  <td>{this.state.resources.title}</td>
                  <td>{this.state.resources.content}</td>
                  <td>{this.state.resources.createdAt}</td>
                  <td>{this.state.resources.updatedAt}</td>
                </tr>
                </tbody>   
                
              </table>
              <Button variant="primary" className="delete" onClick={this.handleSubmit} style={{marginTop: '17px'}}>
                  Delete
                  </Button> 
               
            </div>
          </div>
            </div>
          </div>
    </section>
      </div>
      
    );
  }
}

export default deleteResource;