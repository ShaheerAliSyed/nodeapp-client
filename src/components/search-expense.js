import React, { Component } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Moment from 'react-moment';
import 'moment-timezone';
import './login.css';
class Search extends Component {
    state = {
      search: '',
      expenses: []
    }
    componentDidMount = () => {
        axios.get(`http://localhost:3300/expense`)
          .then( res  => {
            const expenses = res.data.expense;
            console.log(expenses);
            this.setState({
                expenses
              
            })
          })
      }
      updateSearch(event){
        this.setState({
          search: event.target.value.substr(0,20)
          
        });
        
    }
    render() {
      console.log(this.state.search)
      console.log(this.state.expenses, 'ssddsds')
        let filteredExpenses = this.state.expenses.filter(
          (expense) => {
            return expense.title.toLowerCase().indexOf(this.state.search) !== -1;
          }
          
        );
        console.log(filteredExpenses, 'ssd')
      return (
        <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="pageheading">All Expenses</h1>
              </div>
            </div>
          </div>
        </section>
        <div>
          <section className="content">
          <div className="inputSearch" 
        style={{ padding:"5px", width:"255px", marginBottom:"20px",}} 
         >
        <input className="form-control" name="text" type="text" placeholder="Search Expense by Title"
        onChange={this.updateSearch.bind(this)}
        value={this.state.search} />
        <div>
        {filteredExpenses.map((expense) => {
          return <div expense={expense} key={expense.id}/>
        })}
        </div>
        </div>
      <div className="row">
        <div className="col-12">
       
          <div className="card">

          <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                  <thead>
                      <tr>
                      <th>Title</th>
                      <th>Expense Amount</th>
                      <th>User Name</th>
                      <th>Expense Type</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      {filteredExpenses.map(expense => (
                <tr>
                <td>{expense.title}</td>
                <td>{expense.amount}</td>
                <td>{expense.user.name}</td>
                <td>{expense.expenseType.title}</td>
                <td><Moment format="YYYY/MM/DD">{expense.createdAt}</Moment></td>
                <td><Moment format="YYYY/MM/DD">{expense.updatedAt}</Moment></td>
                <td><div className="btn-group btn-group-sm" role="group" aria-label="...">
                      <Button style={{marginRight: '11px',fontSize: "16px"}} className="btn btn-success btn-sm"><Link to={`/editExpense/${expense.id}`}>
                        <i className="fas fa-pencil-alt" style={{color: 'white'}}/></Link></Button>
                      <Button style={{fontSize: "16px"}} style={{color: "black"}} className="btn  btn-danger btn-sm"><Link to={`/deleteExpense/${expense.id}`}>
                        <i className="fas fa-trash-alt" style={{color: 'white'}}/></Link>
                      </Button>
                  </div></td>
                  </tr>
                  ))}
                  </tbody>
          </table>
          </div>
          </div>
      
          </div>
          </div>
          </section>
        </div>
        
        </div>
      )
    }
   }
   
   export default Search