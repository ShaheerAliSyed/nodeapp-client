import React from 'react';
import axios from 'axios';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './login.css';
class createExpenseType extends React.Component {
    constructor(props) {
        super(props);
    this.state = {
    title: '',
    description: '',
  };
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    }
  handleChange = (e) => {
    this.setState({ 
        [e.target.name]: e.target.value},()=>{
        console.log(this.state);
      }); 
  }
  handleCancel = event => {
    this.props.history.push('/searchResourceType');
  }
  handleSubmit = event => {
    // event.preventDefault();

    const expenseType = {
      title: this.state.title,
      description: this.state.description
    };
    console.log(expenseType);
    
    axios.post(`http://localhost:3300/expenseType`,  expenseType )
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/searchExpenseType');
      }).catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Expense Type Registertion Form</h1>
            </div>
          </div>
          </div>
      </section>
        <section className="content">
          <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12">
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Add Expense Type</h3>
  
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i className="fas fa-minus"></i></button>
                </div>
              </div>
              <div className="card-body">
                <form onSubmit={this.handleSubmit} className="usercreationform">
                  <div className="row">
                  <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputName">Expense Type title</label>
                  <input type="text" name="title" placeholder="title"value={this.state.title}
                  onChange={this.handleChange} className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail">Expense Type Description:</label>
                  <input  type="text"  name="description" placeholder="description" value={this.state.description} onChange={this.handleChange}
                  className="form-control"/>
                </div>
                </div>
           
                <div className="col-lg-12">
                <button type="submit" className="btn btn-success float-right" style={{marginLeft: '8px'}}>Submit</button>
                  <button onClick={this.handleCancel} className="btn btn-secondary float-right">Cancel</button>            
                </div>
              </div>
                </form>
                
              </div>       
            </div>
          </div>
        </div>
        </div>
      </section>
      </div>
    )
  }
}
export default createExpenseType;