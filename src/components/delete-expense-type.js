import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class deleteExpense extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expenseTypes: []
    };
} 
  componentDidMount() {
    const id = this.props.expensetypeId
    axios.get(`http://localhost:3300/expenseType/${id}`)
      .then(res => {
        const expenseTypes = res.data.expensetype;
        console.log(expenseTypes);

          this.setState({
            expenseTypes
          })
      })
      .catch((error) => {
        console.log(error)
    }) 
  };
  handleSubmit = event => {
    if (window.confirm("Do you really want to delete?")) {
    event.preventDefault();
    const id = this.props.expensetypeId
    axios.delete(`http://localhost:3300/expenseType/${id}`)
      .then(res => {
        this.props.history.push('/searchExpenseType');
      });
    }
    }
  render() {
    return (
<div className="content-wrapper">
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Delete Expense Type</h1>
          </div>
        </div>
      </div>
    </section>
    <section className="content">
      <div className="row">
        <div className="col-12">
  
          <div className="card">
         
            <div className="card-body">
              <table id="example2" className="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Expense Type ID</th>
                  <th>Expense Type Title</th>
                  <th>Expense Type description</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                </tr>
                </thead>
                
                <tbody>
                <tr>
                  <td>{this.state.expenseTypes.id}</td>
                  <td>{this.state.expenseTypes.title}</td>
                  <td>{this.state.expenseTypes.description}</td>
                  <td>{this.state.expenseTypes.createdAt}</td>
                  <td>{this.state.expenseTypes.updatedAt}</td>
                </tr>
                </tbody>   
                
              </table>
              <Button variant="primary" className="delete" onClick={this.handleSubmit} style={{marginTop: '17px'}}>
                  Delete
                  </Button> 
               
            </div>
          </div>
            </div>
          </div>
    </section>
      </div>
      
      
    );
  }
}

export default deleteExpense;