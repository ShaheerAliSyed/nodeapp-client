import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class EditExpense extends Component {
    constructor(props) {
        super(props);
    this.state = {
        users: [],
        resources: [],
        expenseTypes: [],
        title: '',
        amount: '',
        userId: '',
        resourceId: '',
        expenseTypeId: '',
    };
    
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
};
    handleChange = (e) => {
        this.setState({ 
            [e.target.name]: e.target.value},()=>{
          }); 
      }
  componentDidMount() {
    const id = this.props.expenseId
    axios.get(`http://localhost:3300/expense/${id}`)
      .then(res => {
        const expenses = res.data.expense;
        console.log(expenses);

          this.setState({
            expenses
          })
      })
      .catch((error) => {
        console.log(error)
    })

    axios
     .get('http://localhost:3300/users')
     .then(response => { 
      const users = response.data.user;
          this.setState({
            users
          });
          console.log(users)
     })
     .catch(error => console.log(error.response));

     axios
     .get('http://localhost:3300/resource')
     .then(response => { 
      const resources = response.data.resource;
          this.setState({
            resources
          });
          console.log(resources)
     })
     .catch(error => console.log(error.response));

     axios
     .get('http://localhost:3300/expenseType')
     .then(response => { 
      const expenseTypes = response.data.expensetype;
          this.setState({
            expenseTypes
          });
          console.log(expenseTypes)
     })
     .catch(error => console.log(error.response));

  }
  handleCancel = event => {
    this.props.history.push('/searchExpense');
  }
  handleSubmit = event => {
    event.preventDefault();
    const expense = {
        title: this.state.title,
        amount: this.state.amount,
        userId: this.state.userId,
        resourceId: this.state.resourceId,
        expenseTypeId: this.state.expenseTypeId,
      };
      const id = this.props.expenseId
    axios.put(`http://localhost:3300/expense/${id}`, expense )
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/searchExpense');
      });
    }
  render() {
    // console.log(this.state.users,'jjijii');
    console.log(this.state.users,'jjijii');
    console.log(this.state.resources,'jjijii');
    console.log(this.state.expenseTypes,'jjijii');
    return (

      <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Expense Updation Form</h1>
            </div>
          </div>
          </div>
      </section>
        <section className="content">
          <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12">
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Edit Expense</h3>

                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i className="fas fa-minus"></i></button>
                </div>
              </div>
              <div className="card-body">
                <form onSubmit={this.handleSubmit} className="usercreationform">
                <div className="row">
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputName">Expense Title:</label>
                  <input type="text" name="title" placeholder="title" value={this.state.title}
                  onChange={this.handleChange} className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail"> Expense Amount:</label>
                  <input  type="number"  name="amount" placeholder="amount" value={this.state.amount} onChange={this.handleChange}
                  className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                  <label>
                  Select User:
                  </label>
                  <select className="form-control" name="userId" onChange={this.handleChange}>
                  <option>Click Here</option>
                    {this.state.users.map(user => (
                      <option  key={user.id} value={user.id}>
                        {user.name}
                        {/* {console.log(user.name)} */}
                      </option>
                    ))}
                  </select>
                
                </div>
                </div>
                  <div className="col-lg-12">
                    
                    <button type="submit" className="btn btn-success float-right" style={{marginLeft: '8px'}}>Submit</button>
                    <button onClick={this.handleCancel}  className="btn btn-secondary float-right">Cancel</button>
                  </div>
             
                </div>
                </form>
              </div>       
            </div>
          </div>
        </div>
        </div>
      </section>
      </div>
    );
  }
}

export default EditExpense;