import React, { Component } from 'react';
import axios from 'axios'
import './login.css';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class EditResourceType extends Component {
    constructor(props) {
        super(props);
    this.state = {
        resourceTypes: '',
        title: '',
        description: '',
    };
    
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
};
    handleChange = (e) => {
        this.setState({ 
            [e.target.name]: e.target.value},()=>{
          }); 
      }
  componentDidMount() {
    const id = this.props.resourcetypeId
    axios.get(`http://localhost:3300/resourceType/${id}`)
      .then(res => {
        const resourceTypes = res.data.resourcetype;
        console.log(resourceTypes);

          this.setState({
            resourceTypes
          })
      })
      .catch((error) => {
        console.log(error)
    })
  }
  handleCancel = event => {
    this.props.history.push('/searchResourceType');
  }
  handleSubmit = event => {
    event.preventDefault();
    const resourceType = {
        title: this.state.title,
        description: this.state.description,
      };
      const id = this.props.resourcetypeId
    axios.put(`http://localhost:3300/resourceType/${id}`, resourceType )
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.props.history.push('/searchResourceType');
      });
    }
  render() {
    // console.log(this.state.users,'jjijii');
    console.log(this.state.resourceType,'jjijii');
    return (
<div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1>Resource Type Updation Form</h1>
            </div>
          </div>
          </div>
      </section>
        <section className="content">
          <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12">
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Edit Resource Type </h3>
  
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i className="fas fa-minus"></i></button>
                </div>
              </div>
              <div className="card-body">
                <form onSubmit={this.handleSubmit} className="usercreationform">
                <div className="row">
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputName">Resource Type Title:</label>
                  <input type="text" name="title" placeholder="title" value={this.state.title}
                  onChange={this.handleChange} className="form-control"/>
                </div>
                </div>
                <div className="col-lg-4">
                <div className="form-group">
                  <label for="inputEmail"> Resource Type Description:</label>
                  <input  type="text"  name="description" placeholder="description" value={this.state.description} onChange={this.handleChange}
                  className="form-control"/>
                </div>
                </div>
         
                  <div className="col-lg-12">
                    
                    <button type="submit" className="btn btn-success float-right" style={{marginLeft: '8px'}}>Submit</button>
                    <button onClick={this.handleCancel} className="btn btn-secondary float-right">Cancel</button>
                  </div>
        
                </div>
                </form>
              </div>       
            </div>
          </div>
        </div>
        </div>
      </section>
      </div>

    );
  }
}

export default EditResourceType;